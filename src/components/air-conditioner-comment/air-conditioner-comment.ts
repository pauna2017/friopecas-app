import { Component, Input } from '@angular/core';

@Component({
  selector: 'air-conditioner-comment',
  templateUrl: 'air-conditioner-comment.html'
})
export class AirConditionerCommentComponent {

  @Input() name: string;
  @Input() comment: string;
  @Input() pictureUrl: string;
  @Input() rating: number;

  constructor() {}

}
