import { Component, Input } from '@angular/core';
import { Installer } from './../../models/installer';

/*
  Generated class for the Installer component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'installer',
  templateUrl: 'installer.html'
})
export class InstallerComponent {

  @Input() installer: Installer;

  constructor() {
  }

}
