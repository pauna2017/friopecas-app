import { NativeStorage } from '@ionic-native/native-storage';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class NativeStorageMock extends NativeStorage {
    setItem(reference: string, value: any): Promise<any> {
        return new Promise((resolve, reject) => {
            localStorage.setItem(reference, JSON.stringify(value));
            resolve(true);
        });
    }
    getItem(reference: string): Promise<any> {
        return new Promise((resolve, reject) => {
            let value = localStorage.getItem(reference);
            if(value) {
                let valueToReturn = JSON.parse(value);
                resolve(valueToReturn);
            }else {
                resolve(undefined);
            }
        });
    }
    remove(reference: string): Promise<any> {
        return new Promise((resolve, reject) => {
            localStorage.removeItem(reference);
            resolve(true);
        });
    }
}