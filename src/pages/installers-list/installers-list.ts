import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import 'rxjs'

import { Installer } from './../../models/installer';
import { InstallerService } from '../../providers/installer-service';

@Component({
  selector: 'page-installers-list',
  templateUrl: 'installers-list.html'
})
export class InstallersListPage {

  public installersList: Installer[];  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public installerService:InstallerService,
    public loadingCtrl:LoadingController 
  ){}

  ionViewDidLoad() {    
    this.getInstallersList();    
  }

  private getInstallersList():void{
    let loader = this.loadingCtrl.create({content: 'Carregando instaladores...'});    
    loader.present();

    this.installerService.getInstallersList()
      .finally(() => loader.dismiss() )
      .map(data => data.json())      
      .subscribe(
          data => {
            this.installersList = data;
          },
          error => console.log('err', error)
      )      
  }
}
