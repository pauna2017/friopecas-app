import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

import { AirConditioner } from './../../models/airConditioner';
import { InstallersListPage } from './../installers-list/installers-list';
import { AirConditionerDetailPage } from './../air-conditioner-detail/air-conditioner-detail';

@Component({
  selector: 'page-air-conditioner-list',
  templateUrl: 'air-conditioner-list.html'
})
export class AirConditionerListPage {

  public btus: number;
  public airConditionerList: AirConditioner[] =
    [
      {
        pictureUrl: `assets/ar1.jpg`,
        name: `Split Samsung`,
        price: `R$ 1200,99`,
        description: `Ideal para o seu escritório.`,
        rating: 5
      },
      {
        pictureUrl: `assets/ar2.jpg`,
        name: `Ar Condicionado Portátil`,
        price: `R$ 2200,99`,
        description: `Em qualquer lugar da casa`,
        rating: 4.5
      },
      {
        pictureUrl: `assets/ar3.jpg`,
        name: `Split Hi Wall Eco Plus`,
        price: `R$ 999,99`,
        description: `Conheça a função "i feel"`,
        rating: 4
      },
      {
        pictureUrl: `assets/ar4.jpg`,
        name: `Ar Condicionado - Elgin`,
        price: `R$ 1750,99`,
        description: `Respire um ar com qualidade.`,
        rating: 3.5
      },
      {
        pictureUrl: `assets/ar5.jpg`,
        name: `Portátil Electrolux`,
        price: `R$ 2280,99`,
        description: `Facilidade dentro da sua casa.`,
        rating: 4.5
      },
  ]; 
  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private alertCtrl: AlertController
    ) {
    if(navParams.get('isLogin')){
      alertCtrl.create({
        title: 'Bem vindo!',
        message: 'Aqui você encontrará os melhores ar condicionados. Caso tenha dúvida use nosso simulador ou contate um instalador.',
        buttons: ['Ok']
      }).present();
    }
    
    this.btus = navParams.get('btus');
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AirConditionerListPage');
  }

  public goToInstallersList():void{
    this.navCtrl.push(InstallersListPage);
  }

  public goToDetailPage( _airConditioner ):void{
    this.navCtrl.push(AirConditionerDetailPage, {
      airConditioner: _airConditioner
    });
  }

}
