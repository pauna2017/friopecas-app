import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

/*
  Generated class for the CommentsModal page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-comments-modal',
  templateUrl: 'comments-modal.html'
})
export class CommentsModalPage {

  public comments:any = [
    {
      name: 'Thiago Pereira', 
      comment: 'Excelente produto. Está refrigerando perfeitamente o meu escritório onde trabalham 7 pessoas',
      rating: 5,
      pictureUrl:'https://randomuser.me/api/portraits/thumb/men/25.jpg'
    },
    {
      name: 'Izabela Cristina',
      comment: 'Bom, porém, até mesmo o instalador teve dificuldades na instalação',
      rating: 4.5,
      pictureUrl:'https://randomuser.me/api/portraits/thumb/women/33.jpg'
    } ,
    {
      name: 'Jonatas',
      comment: 'Um ótimo ar condicionado, instalação simples, eu mesmo instalei no meu quarto.',
      rating: 5,
      pictureUrl:'https://randomuser.me/api/portraits/thumb/men/17.jpg'
    },
    {
      name: 'Cristiano Dias',
      comment: 'Deixou a desejar, não recomendo este produto. Não está refrigerando o suficiente no galpão onde trabalho',
      rating: 1.5,
      pictureUrl:'https://randomuser.me/api/portraits/thumb/men/47.jpg'
    }
  ];
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController) {}

  public dismiss():void{
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommentsModalPage');
  }


}
