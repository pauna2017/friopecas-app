import { SignupPage } from './../signup/signup';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';


import { MyToast } from './../../providers/mytoast';
import { LoginService } from './../../providers/login-service';
import { AirConditionerListPage } from '../air-conditioner-list/air-conditioner-list';
import { UserLogin } from './../../models/userLogin';
import { AuthService } from './../../providers/auth-service';
import { Credentials } from './../../models/credentials';
/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  public credential: Credentials = {
    email: '',
    password: ''
  };

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private loadingCtrl: LoadingController,
    private loginService: LoginService,
    private myToast:MyToast,
    private authService:AuthService,
    private alertCtrl: AlertController) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  public login(form, invalidEmail):void{
    if(invalidEmail){
      this.myToast.showToaster('Email inválido.', 2000);
      return;
    }
    if(!form.valid){
      this.myToast.showToaster('Email e senha são obrigatórios.', 2000);
      return;
    }

    const loader:Loading = this.presentLoading();
    this.loginService.login(this.credential)
      .finally(() => {
        loader.dismiss();
      })
      .map(data => <UserLogin> data.json())
      .subscribe(
        data => {          
          if(data['installer']){
            this.myToast.showToaster('Instaladores devem acessar somente o aplicativo de instaladores. Este aplicativo é restrito para clientes.');
            return;
          } 
          this.authService.login(data);
          this.navCtrl.setRoot(AirConditionerListPage, {isLogin: true});
        },
        error => {
          if(error.statusText == 'Unauthorized'){
            this.myToast.showToaster('E-mail ou senha incorretos.', 5000);
          } else {
            this.myToast.showToaster('Um erro inesperado aconteceu, tente novamente mais tarde.', 5000);
          }
        }
      );
      
    
  }

  public forgotPassword():void {
    this.alertCtrl.create({
      title: 'Esqueceu sua senha?',
      message: 'Digite o seu e-mail abaixo e dentro de alguns minutos você receberá sua senha.',
      inputs: [
        {
          name: 'email',
          placeholder: 'Seu email',
          type: 'email'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Ok',
          handler : data => {
            console.log(`Enviar e-mail para ${data.email}`);
          }
        }
      ]
    }).present();
  }

  public goSignup(): void {
    this.navCtrl.push(SignupPage);
  }

  private presentLoading(): Loading {
    let loading = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loading.present();
    return loading;
  }

}
