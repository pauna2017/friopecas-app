import { AirConditionerListPage } from './../air-conditioner-list/air-conditioner-list';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

import { MyToast } from './../../providers/mytoast';
/*
  Generated class for the Simulator page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-simulator',
  templateUrl: 'simulator.html'
})
export class SimulatorPage {

  public simulator = {
    width: null,
    length: null,
    numPersons: null,
    sunIncidence: null,
    numWindow: null,
    numEletronics: null
  }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public myToast: MyToast,
    public alertCtrl: AlertController ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SimulatorPage');
  }

  public calculate(form):void{    
    const area = this.simulator.width * this.simulator.length;  
    if(!area){
      this.myToast.showToaster('A área é obrigatória. Preencha largura e comprimento para continuar.');
      return;
    }

    let btus = area * 600;

    if(this.simulator.sunIncidence == 1){
        btus += 600;
    } else if (this.simulator.sunIncidence == 2){
        btus += 650;      
    } else if (this.simulator.sunIncidence == 3){
        btus += 700;
    }

    if(this.simulator.numPersons){
      btus += this.simulator.numPersons * 600;
    }

    if(this.simulator.numWindow){
      btus += this.simulator.numWindow * 300;
    }

    if(this.simulator.numEletronics){
      btus += this.simulator.numEletronics * 600;
    }
    
    this.alertCtrl.create({
      message: `O ar condicionado ideal para você deve ter no mínimo <b>${btus} btus.</b> Deseja visualizar ar condicionados com essas características?`,
      buttons: [
        {text: 'Sim' , handler: () => {
          this.navCtrl.setRoot(AirConditionerListPage, {btus: btus});
        }},
        {text: 'Não', role: 'cancel'}        
      ]
    }).present();
  }

}
