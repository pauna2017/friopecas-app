import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ViewController } from 'ionic-angular';

import { Installer } from './../../models/installer';
import { InstallerService } from './../../providers/installer-service';

@Component({
  selector: 'page-installers-modal',
  templateUrl: 'installers-modal.html'
})
export class InstallersModalPage {

  public installersList:Installer[];
  public loaded: boolean;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public installerService: InstallerService,
    public loadingCtrl: LoadingController,
    public viewCtrl: ViewController) {}

  ionViewDidLoad() {    
    this.getInstallersList();        
  }

  private getInstallersList():void{
    this.installerService.getInstallersList()
      .finally(() => this.loaded = true)
      .map(data => data.json())          
      .subscribe(
          data => this.installersList = data.slice(0, 3),
          error => console.log('err', error)
      );      
  }

  public dismiss():void{
    this.viewCtrl.dismiss();
  }

}
