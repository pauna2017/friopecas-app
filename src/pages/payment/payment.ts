import { AirConditionerListPage } from './../air-conditioner-list/air-conditioner-list';
import { InstallersListPage } from './../installers-list/installers-list';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';

import { CardIO } from '@ionic-native/card-io';
import { AirConditioner } from './../../models/airConditioner';

@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html'
})
export class PaymentPage {

  public airConditioner: AirConditioner;
  private optionsToScanCard: any;
  public creditCard:any = {
  };
  public dtValidate:string;
  constructor(public navCtrl: NavController,
   public navParams: NavParams,
   private cardIO: CardIO,
   private alertCtrl:AlertController,
   private loadingCtrl:LoadingController,
   private toastCtrl:ToastController) {
   }

  ionViewDidLoad() {
    this.canScanCard();
  }

  private canScanCard():void{
    this.cardIO.canScan()
      .then(
        (res: boolean) => {
          if(res){
            this.optionsToScanCard = {
              requireExpiry: true,
              requireCCV: false,
              requirePostalCode: false
            };            
          }
        }
      )
      .catch(error => console.log)
  }

  public scanCard():void{
    this.cardIO.scan(this.optionsToScanCard)
        .then(data => {
          console.log(data);
          this.creditCard.cardNumber = 
            data.cardNumber.substr(0,4) + ' ' + data.cardNumber.substr(4, 4) + ' ' + data.cardNumber.substr(8, 4) + ' ' + data.cardNumber.substr(12, 4); 
          this.dtValidate = (data.expiryMonth.toString().length == 2?data.expiryMonth:`0${data.expiryMonth}`) + '/' + data.expiryYear.toString().substr(2,2);

        })
        .catch(error => {
          console.log('error', error);
        });
  }

  public backPage():void{
    this.navCtrl.pop();
  }

  public finishPayment():void{
    let loading =  this.loadingCtrl.create({content: 'Processando pagamento...', duration: 4000})
    loading.present();

    loading.onDidDismiss(() => {
        this.alertCtrl.create({
          title: `Pagamento confirmado`,
          message: `Pedido realizado com sucesso. Seu Ar condicionado será entregue em até 5 dias úteis. Em breve será enviado para seu e-mail um 
          código de rastreamento. Recomendamos que contrate um de nossos instaladores parceiros para realizar a instalação. Tem interesse em encontrar um instalador?`,
          buttons: [
            {
              text: 'Sim',
              handler: () => {
                this.navCtrl.popToRoot({
                  animate: true,
                  animation: 'md-transition'
                }).then(() => this.navCtrl.push(InstallersListPage));
                this.toastCtrl.create({
                  message:'Obrgado por comprar com a FrioPeças. Estamos a disposição em caso de qualquer dúvida.',  duration:4000
                }).present();
              }
            },
            {
              text: 'Não',
              handler: () => {
                this.navCtrl.setRoot(AirConditionerListPage);
                this.toastCtrl.create({
                  message:'Obrgado por comprar com a FrioPeças. Estamos a disposição em caso de qualquer dúvida.',  duration:4000
                }).present();
              }
            }
          ]
        }).present();
    })
      
      
  }

}
