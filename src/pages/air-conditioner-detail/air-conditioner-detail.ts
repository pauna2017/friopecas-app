import { CommentsModalPage } from './../comments-modal/comments-modal';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';


import { PaymentPage } from './../payment/payment';
import { AirConditioner } from './../../models/airConditioner';
import { InstallersModalPage } from './../installers-modal/installers-modal';

@Component({
  selector: 'page-air-conditioner-detail',
  templateUrl: 'air-conditioner-detail.html'
})
export class AirConditionerDetailPage {

  public airConditioner:AirConditioner;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController) {
    this.airConditioner = this.navParams.get('airConditioner');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AirConditionerDetailPage');
  }

  public openInstallersModal():void{
    this.modalCtrl.create(InstallersModalPage).present();
  }

  public openCommentsModal():void{
    this.modalCtrl.create(CommentsModalPage).present();
  }

  public backPage():void{
    this.navCtrl.pop();
  }

  public goToPaymentPage():void{
    this.navCtrl.push(PaymentPage, {
      airConditioner: this.airConditioner
    });
  }

}
