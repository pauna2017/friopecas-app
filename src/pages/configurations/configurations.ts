import { UserLogin } from './../../models/userLogin';
import { AuthService } from './../../providers/auth-service';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/*
  Generated class for the Configurations page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-configurations',
  templateUrl: 'configurations.html'
})
export class ConfigurationsPage {

  public userData: UserLogin;
  public loaded: boolean;
  public notification: boolean = true;
  public lastSearchs: boolean = true;

  constructor(public navCtrl: NavController,
    public   navParams: NavParams,
    private authService: AuthService) {
      authService.getToken()
        .then(user => {
          this.userData = user;
          this.loaded = true;
        });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfigurationsPage');
  }

}
