import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, AlertController, LoadingController, Loading } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ClientService } from './../../providers/client-service';
import { AuthService } from './../../providers/auth-service';
import { AirConditionerListPage } from './../air-conditioner-list/air-conditioner-list';
/*
  Generated class for the Signup page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {

  public photoBase64: string;
  public signupForm: FormGroup;
  public submitted: boolean;

  constructor(public navCtrl: NavController,
   public navParams: NavParams,
   public actionSheetCtrl: ActionSheetController,
   private camera: Camera,
   private formBuilder: FormBuilder,
   private alertCtrl: AlertController,
   private loadingCtrl: LoadingController,
   private clientService: ClientService,
   private authService: AuthService) {
     this._initForm();
   }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  public getPhoto(): void {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'O que deseja?',
      buttons: [
        {
          text: 'Utilizar a câmera',
          handler: () => {this._getPhoto(this.camera.PictureSourceType.CAMERA)},
          icon: 'camera'
        },
        {
          text: 'Selecionar do álbum',
          handler: () => {this._getPhoto(this.camera.PictureSourceType.PHOTOLIBRARY)},
          icon: 'images'
        }
      ] 
    });

    actionSheet.present();
  }

  private _getPhoto(sourceType): void{
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      cameraDirection: 1,
      saveToPhotoAlbum: true,
      correctOrientation: true,
      targetHeight: 250,
      targetWidth: 250,
      allowEdit: true,
      sourceType: sourceType
    }
    this.camera.getPicture(options).then(imageData => {
      console.log('imageData', imageData);
      this.photoBase64 = 'data:image/jpeg;base64,' + imageData;
      this.signupForm.get('photo').setValue(imageData);
    }, err => {
      
    });
  }

  public signup(formValue: any, valid: boolean): void {
    if(!formValue.photo){
      this._alertPhoto(formValue);
    } else {
      this._signup(formValue);  
    }
  }

  private _signup(clientToRegister): void {
    this.submitted = true;
    const loader = this.presentLoading('Registrando...');
    this.clientService.register(clientToRegister)
      .map(res => res.json())
      .finally(() => loader.dismiss())
      .subscribe(
        data => {
          if(data.client){
            this.authService.login(data);
            this.navCtrl.setRoot(AirConditionerListPage, {isLogin: true});
          }
        },
        error => {
          let msg;
          error = JSON.parse(error['_body']).error;
          switch(error['title']) {
            case 'EmailAlreadyExists':
              msg = 'Este email já está sendo utilizado.';
              break;
            default:
              msg = 'Um erro inesperado aconteceu.'
              break;
          }
          this.alertCtrl.create({
            title: 'Desculpe...',
            message: msg,
            buttons: [{text: 'Ok', role: 'cancel'}]
          }).present();
        }
      );
  }

  private _alertPhoto(formValue: any): void{
    this.alertCtrl.create({
      title: 'Tem certeza?',
      message: 'Você ainda não inseriu uma foto. Tem certeza que deseja concluir o seu cadastro sem adicionar uma foto?',
      buttons: [{
        text: 'Não',
        handler: () => {
          this.getPhoto();
        }
      },
      {
        text: 'Sim',
        role: 'cancel',
        handler: () => {          
          this._signup(formValue);
        }
      }]
    }).present();
  }

  private presentLoading(msg): Loading {
    const loading = this.loadingCtrl.create({
      content: msg
    });
    loading.present();
    
    return loading;
  }

  private _initForm(): void {
    this.signupForm = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(45)]],
      lastName: ['', [Validators.required]],
      password: ['', [Validators.required]],
      email: ['', [Validators.required]],
      photo: [''],
      street: ['', [Validators.required]],
      streetNumber: ['', [Validators.required]],
      city: ['', [Validators.required]],
      state: ['', [Validators.required]],
      district: ['', [Validators.required]],
      zipCode: ['', [Validators.required]],
    });
   
  }

}
