export interface Token {
    accessToken: string;
    idClient: string;
    idUser: string;
}