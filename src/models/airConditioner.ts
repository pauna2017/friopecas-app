export interface AirConditioner{
    pictureUrl: string
    name: string
    price: string
    description: string
    rating: number
}