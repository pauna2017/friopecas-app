export interface Installer{
   rating:number
   photo:string
   city:string
   codigoFp: string
   state: string
   user: {
    firstName: string,
    lastName: string,
    email: string
   }
}