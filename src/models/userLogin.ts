export interface UserLogin {
  token: string,
  client: {
    _id: string,
    user: {
      _id: string,
      updatedAt: string,
      createdAt: string,
      email: string,
      firstName: string,
      lastName: string,
      role: string
    },
    photo: string,
    street: string,
    streetNumber: string,
    city: string,
    state: string,
    district: string,
    zipCode: string
  }
}