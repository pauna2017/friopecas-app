import { UserLogin } from './../models/userLogin';
import { Injectable, EventEmitter } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';
import 'rxjs/add/operator/toPromise';

const AUTH_DATA = 'LOCAL_STORAGE_FRIOPECAS_APP';

@Injectable()
export class AuthService {

  public logged: boolean = false;

  public loginDone = new EventEmitter<UserLogin>();
  public logoutDone = new EventEmitter<any>();

  constructor(private nativeStorage: NativeStorage) {}

  public login(user: UserLogin): void {
    this.nativeStorage.setItem(AUTH_DATA, user);
    this.loginDone.emit(user);
  }

  public logout(): void {
    this.nativeStorage.remove(AUTH_DATA);
    this.logoutDone.emit();
  }

  public getToken(): Promise<UserLogin> {
    return this.nativeStorage.getItem(AUTH_DATA);
  }
}