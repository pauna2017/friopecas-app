import { AuthService } from './auth-service';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { server } from './../enviromment/enviromment';
import 'rxjs/add/operator/map';
/*
  Generated class for the InstallerService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class InstallerService {

  // private randomUserService = 
  //   'https://randomuser.me/api/?results=40&inc=gender,name,location,email,phone,cell,picture';

  constructor(public http: Http,
  public authServicer: AuthService) {
    console.log('Hello InstallerService Provider');
  }

  // public getInstallersList():Observable<any>{
  //   return this.http.get(this.randomUserService);      
  // }

  public getInstallersList(): Observable<any>{
    return this.http.get(`${server}/installer`);
  }

}
