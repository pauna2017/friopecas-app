import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { server } from './../enviromment/enviromment';

/*
  Generated class for the ClientService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ClientService {

  constructor(public http: Http) {
    console.log('Hello ClientService Provider');
  }
  
  public register(clientRegister: any): Observable<any>{
    return this.http.post(`${server}/client`, clientRegister);
  }

}
