import { SignupPage } from './../pages/signup/signup';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NativeStorage } from '@ionic-native/native-storage';
import { NativeStorageMock } from './../mocks/NativeStorageMock';

// Pages
import { LoginPage } from '../pages/login/login';
import { AirConditionerListPage } from '../pages/air-conditioner-list/air-conditioner-list';
import { AirConditionerDetailPage } from '../pages/air-conditioner-detail/air-conditioner-detail';
import { CommentsModalPage } from './../pages/comments-modal/comments-modal';
import { InstallersListPage } from '../pages/installers-list/installers-list';
import { InstallersModalPage } from '../pages/installers-modal/installers-modal';
import { SimulatorPage } from '../pages/simulator/simulator';
import { ConfigurationsPage } from '../pages/configurations/configurations';
import { PaymentPage } from './../pages/payment/payment';
// Minhas classes
import { InstallerService } from '../providers/installer-service';
import { AuthService } from './../providers/auth-service';
import { LoginService } from './../providers/login-service';
import { ClientService } from './../providers/client-service';
import { CapitalizePipe } from '../pipes/capitalize.pipe';
import { MyToast } from './../providers/mytoast';

//Components
import { InstallerComponent } from '../components/installer/installer';
import { AirConditionerCommentComponent } from './../components/air-conditioner-comment/air-conditioner-comment';

// Libs de terceiro
import { Ionic2RatingModule } from 'ionic2-rating';
import { CardIO } from '@ionic-native/card-io';
import { Camera } from '@ionic-native/camera';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    SignupPage,
    AirConditionerListPage,
    AirConditionerDetailPage,
    InstallersListPage,
    InstallersModalPage,
    SimulatorPage,
    ConfigurationsPage,
    CapitalizePipe,
    InstallerComponent,
    AirConditionerCommentComponent,
    CommentsModalPage,
    PaymentPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    Ionic2RatingModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    SignupPage,
    AirConditionerListPage,
    AirConditionerDetailPage,
    InstallersListPage,
    InstallersModalPage,
    SimulatorPage,
    ConfigurationsPage,
    InstallerComponent,
    AirConditionerCommentComponent,
    CommentsModalPage,
    PaymentPage
  ],
  providers: [
    Camera,
    StatusBar,
    SplashScreen,
    InstallerService,
    AuthService,
    LoginService,
    ClientService,
    MyToast,
    CardIO,
    NativeStorage,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide: NativeStorage, useClass: NativeStorageMock}
  ]
})
export class AppModule {}
