import { AirConditionerListPage } from './../pages/air-conditioner-list/air-conditioner-list';
import { UserLogin } from './../models/userLogin';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { InstallersListPage } from '../pages/installers-list/installers-list';
import { SimulatorPage } from '../pages/simulator/simulator';
import { ConfigurationsPage } from '../pages/configurations/configurations';
import { AuthService } from './../providers/auth-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;
  
  pages: Array<{title: string, component: Component, icon: string}>;
  userData: UserLogin;
  urlPhoto: string;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
  public authService : AuthService) {
    this.initializeApp();
    this.checkLogin();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.backgroundColorByHexString('#09426f');
        // used for an example of ngFor and navigation
      this.pages = [
        { title: 'Instaladores Parceiros', component: InstallersListPage, icon: 'people'},
        { title: 'Simulador', component: SimulatorPage, icon: 'calculator'},
        { title: 'Configurações', component: ConfigurationsPage, icon: 'settings'}      
      ];
      
      this.authService.loginDone.subscribe(user => {
        this.userData = user;      
      });

      this.authService.logoutDone.subscribe(() => {
        this.userData = null;
      });

      this.splashScreen.hide();
    });
  }

  checkLogin(): void {
    this.authService.getToken()
      .then(data => {
        if(data && data.token){
          this.nav.setRoot(AirConditionerListPage);
          return this.authService.getToken();          
        }
      })
      .then(userData => {
        this.userData = userData;
      })
      .catch(error => {
        console.log('error', error);
      })
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.push(page.component);
  }

  public logout():void {
    this.authService.logout();
    this.nav.setRoot(this.rootPage);
  }
}
